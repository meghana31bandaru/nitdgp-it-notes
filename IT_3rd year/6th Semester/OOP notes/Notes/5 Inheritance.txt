Inheritance
	-The mechanism by which object of one class acquires the properties of object of some other class
	-Basically new classes are created and they acquire the properties of existing classes and also add some new properties of their own
	-The existing class is known as the Base class
	-The new class is known as the derived class 
	-The derived class acquires some or all properties from the base class
	-A derived class can also inherit properties from more than one class
	-There are 5 different types of inheritance
		-Single: Derived class with only base class
		-Multiple: Derived class with more than one base class
		-Hierarchical: One base class may be inherited by multiple derived classes
		-Multilevel: A class is derived from another derived class
		-Hybrid: Combination of hierarchical, multilevel and multiple
	-Defining Derived Class
		-A derived class can be defined by specifying its relationship with the base class together with its own details
		-class derived-classname:visibility-mode base-classname{
		 // members of derived class
		};
		-Visibility mode specifies the access specifier. It may be either public or private by default
	-Protected access specifiers
		-A member declared as protected is accessible by member functions within its class and any class immediately derived from it
		-It cannot be accessed by the functions outside these two classes
	-Virtual Base Class
		-Consider a case where multilevel, multiple and hierarchical inheritance happened together
		-All the public and protected members of class A are inherited into C twice, one set via B1, another set of same properties by B2
		-To avoid duplication of inherited members due to these multiple path, the common base class have to be inherited as virtual class
		-Virtual base class ensures that upon inheritance, only one copy of it will be copied
	-Abstract Class
		-Class that is not used to create objects
		-It can act only as a base class
	-Constructors in Derived Classes
		-If the base class constructor takes no argument then it is not required for the derived class to have constructor
		-If the base class constructor takes any argument then it is mandatory for the derived class to have constructor with one or more arguments and they pass the arguments to the base class constructor
		-When both the derived class and base classes contain constructors then base constructor is executed first then the derived class constructor is executed
		-The base classes are constructed in the order in which they appear in the declaration of the derived class
		-The constructor will be executed in order of inheritance
		-derived constructor(arglst1, arglst2,...argelistN) :
		base1(arglst1),base2(arglst2),….,baseN(arglstN)
		{
			//Body of default constructor
		}