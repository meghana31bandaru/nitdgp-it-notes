#O(n^1.585)
def karatsuba(num1,num2):
	if len(str(num1)) <= 2 or len(str(num2)) <= 2:
		return num1*num2
	exp = 10**(len(str(num1))/2)
	x1 = num1/exp
	y1 = num2/exp
	x2 = num1%exp
	y2 = num2%exp
	a = karatsuba(x1,y1)
	c = karatsuba(x2,y2)
	b = karatsuba((x1 + x2),(y1 + y2)) - a - c
	return karatsuba(exp,exp)*a + exp*b + c

def mul(num1, num2):
	num1=str(num1)
	num2=str(num2)
	l=len(num1)
	n1 = l*[0]
	n2 = l*[0]
	for i in range(0, l):
		n1[i]=int(num1[i])
		n2[i]=int(num2[i])
	ans = [0]*(l*2)
	ptr = 0
	for i in range(1,l+1):
		k = ptr-1
		ptr -= 1
		carry=0
		for j in range(1,l+1):
			tmp = (n1[-i]*n2[-j]+carry+ans[k])
			ans[k]=tmp%10
			carry=tmp/10
			k -= 1
		ans[k]=carry
	product=0
	for i in range(0, 2*l):
		product = product*10 + ans[i]
	return product

num1 = int(input())
num2 = int(input())
print(num1*num2)
print(mul(num1, num2))	
print(karatsuba(num1, num2))
