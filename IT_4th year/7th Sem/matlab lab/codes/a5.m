clc;
a=[90,75,75,80;35,85,55,65;125,95,90,105;45,110,95,115];
tmp=a;
for i=1:size(a,1)
    sub=min(a(i,:));
    a(i,:) = a(i,:)-sub;
end
for i=1:size(a,2)
    sub=min(a(:,i));
    a(:,i) = a(:,i)-sub;
end
while true
    b=a;
    lines = 0;
    while true
        minZ=inf;
        for i=1:size(b,1)
            count=size(find(b(i,:)==0),2);
            if(count>0 && count < minZ)
                minZ=count;
                d=1;
                index=find(b(i,:)==0,1);
            end
        end
        for i=1:size(b,2)
            count=size(find(b(:,i)==0),1);
            if(count>0 && count < minZ)
                minZ=count;
                d=0;
                index=find(b(:,i)==0,1);
            end
        end
        if minZ==inf
            break;
        end
        if d==1
            b(:,index)=inf;
        else
            b(index,:)=inf;
        end
        lines = lines + 1;
    end
    sub = min(min(b));
    if sub~=inf
        for i=1:size(a,1)
            for j=1:size(a,2)
                if(b(i,j)~=inf)
                    a(i,j) = a(i,j)-sub;
                elseif(size(find(b(i,:)==inf),2)==4 && size(find(b(:,j)==inf),1)==4)
                    a(i,j) = a(i,j)+sub;
                end
            end
        end
    end
    if(lines==4)
        break;
    end
end  
disp(a);